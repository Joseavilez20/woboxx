import { Component, OnInit } from '@angular/core';

declare function collapse_nav(head:any, toggler:any, sidenav:any, main_:any): any;
declare function reportWindowSize():any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() {
    this.loadScripts();
   }

  ngOnInit(): void {
  }

  loadScripts() {
    const externalScriptArray = [
      'assets/js/custom.js'
    ];
    for (let i = 0; i < externalScriptArray.length; i++) {
      const scriptTag = document.createElement('script');
      scriptTag.src = externalScriptArray[i];
      scriptTag.type = 'text/javascript';
      scriptTag.async = false;
      scriptTag.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(scriptTag);
    }
  }

}
