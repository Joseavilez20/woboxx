import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor( public modal: NgbModal ) { }

  ngOnInit(): void {
  }

  openVerticallyCentered(content: any) {
    this.modal.open(content, { centered: true });
  }

}
