import { Component } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'woboxx';
  constructor(private modalService: NgbModal) { }

  // open(content: any) {
  //     this.modalService.open(content);
  // }

}
