import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
// import { MatToolbarModule } from '@angular/material/toolbar';
// import { MatButtonModule } from '@angular/material/button';
// import { MatSidenavModule } from '@angular/material/sidenav';
// import { MatIconModule } from '@angular/material/icon';
// import { MatListModule } from '@angular/material/list'
// import { MatGridListModule } from '@angular/material/grid-list';
// import { MatCardModule } from '@angular/material/card';
// import { MatMenuModule } from '@angular/material/menu';
// import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
// import {MatSliderModule} from '@angular/material/slider';
// import {MatChipsModule} from '@angular/material/chips';
// import {MatInputModule} from '@angular/material/input';
// import {MatFormFieldModule} from '@angular/material/form-field';
// import {MatDividerModule} from '@angular/material/divider';
// import {MatSelectModule} from '@angular/material/select';
// import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';
// import {MatExpansionModule} from '@angular/material/expansion';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from './pages/register/register.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ChatMovilComponent } from './pages/chat-movil/chat-movil.component';
import { ChatComponent } from './pages/chat/chat.component';
import { DashboardComponent } from './shared/dashboard/dashboard.component';
import { ProfilePostComponent } from './pages/profile-post/profile-post.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterComponent,
    FooterComponent,
    ProfileComponent,
    ChatMovilComponent,
    ChatComponent,
    DashboardComponent,
    ProfilePostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    // MatToolbarModule,
    // MatButtonModule,
    // MatSidenavModule,
    // MatIconModule,
    // MatListModule,
    // MatGridListModule,
    // MatCardModule,
    // MatMenuModule,
    // MatBottomSheetModule,
    // MatSliderModule,
    // MatChipsModule,
    // MatInputModule,
    // MatFormFieldModule,
    // MatToolbarModule,
    // MatDividerModule,
    // MatSelectModule,
    // MatSlideToggleModule,
    FormsModule,
    // MatExpansionModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
